const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('combined'));

const auth = require('./src/routes/auth.route');
app.use('/api/auth', auth);

const user = require('./src/routes/user.route');
app.use('/api/user', user);

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({message: error.message || 'Server error'});
});

// eslint-disable-next-line
async function setup() {
  await mongoose.connect(
      'mongodb+srv://slavik:slavik123@cluster0-vwanw.mongodb.net/test?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );

  const PORT = process.env.PORT || 8081;
  app.listen(PORT, () => console.log('Server listen on port', PORT));
}

setup();
