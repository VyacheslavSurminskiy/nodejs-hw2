const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../../constants');

const {User} = require('../models/user.model');

module.exports = {
  me: async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, JWT_SECRET);

    const {_id, username, createdDate} = await User.findById(decoded._id);

    res.send({
      user: {
        _id,
        username,
        createdDate,
      },
    });
  },
  delete: async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, JWT_SECRET);

    await User.findByIdAndRemove(decoded._id);

    res.send({message: 'Success'});
  },
  changePassword: async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const decoded = jwt.verify(token, JWT_SECRET);

    const {password} = await User.findById(decoded._id);

    const {oldPassword, newPassword} = req.body;

    const isPasswordCorrect = await bcrypt.compare(oldPassword, password);

    if (!isPasswordCorrect) {
      return res.status(400).json({message: 'Wrong old password'});
    }

    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await User.updateOne({_id: decoded._id}, {password: hashedPassword});

    res.send({message: 'Password has been updated'});
  },
};
