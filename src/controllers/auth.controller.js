const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../../constants');

const {User} = require('../models/user.model');

module.exports = {
  register: async (req, res) => {
    const {username, password} = req.body;

    const hashedPassword = await bcrypt.hash(password, 10);

    const user = new User({
      username,
      password: hashedPassword,
    });

    await user.save();

    res.send({message: 'Success'});
  },
  login: async (req, res) => {
    const {username, password} = req.body;

    const user = await User.findOne({username});

    if (!user) {
      return res.status(400).json({message: `'${username}' not found!`});
    }

    const isPasswordCorrect = await bcrypt.compare(password, user.password);

    if (!isPasswordCorrect) {
      return res.status(400).json({message: 'Wrong password!'});
    }

    // eslint-disable-next-line
    const jwt_token = jwt.sign(
        {
          username: user.username,
          _id: user._id,
        },
        JWT_SECRET,
    );

    res.send({message: 'Success', jwt_token});
  },
};
