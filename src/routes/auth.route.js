const express = require('express');
// eslint-disable-next-line
const router = express.Router();

const AuthController = require('../controllers/auth.controller');

router.post('/register', AuthController.register);
router.post('/login', AuthController.login);

module.exports = router;
