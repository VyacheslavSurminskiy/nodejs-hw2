const express = require('express');
// eslint-disable-next-line
const router = express.Router();

const UserController = require('../controllers/user.controller');

router.get('/me', UserController.me);
router.delete('/me', UserController.delete);
router.patch('/me', UserController.changePassword);

module.exports = router;
